# hosts

hosts files that can be added to your Pi-hole (for more info see on https://pi-hole.net/ )

## fsma
address : https://gitlab.com/Spiroo/hosts/-/raw/main/fsma

fsma : Financial Services and Markets Authority

This hosts file is a collection of hosts grabbed from https://www.fsma.be/en/warnings/companies-operating-unlawfully-in-belgium

The list includes:
- companies that offer, in or from Belgium, financial services and products without complying with Belgian financial legislation (no authorization / failure to publish a prospectus / etc.);
- companies about which, in addition to any infringements of the financial legislation and regulations the FSMA supervises, the latter has identified serious evidence of investment fraud;
- companies behind 'recovery room' fraud.
